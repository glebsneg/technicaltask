const
    info = document.getElementById('info'),
    popover = document.getElementById('popoverBox');
    close = document.getElementById('close');

function showPopover() {
    popover.classList.add('visible');
}

function hidePopover() {
    popover.classList.remove('visible');
}

info.onclick = function () {
    showPopover();
};

close.onclick = function () {
    hidePopover();
};
