const
    preloaderButton = document.getElementById('preloaderButton'),
    preloader = document.getElementById('preloader'),
    textBlock = document.getElementById('textBlock'),
    tabText = document.getElementById('tabText');

//  Show or hide block
function showBlock(id) {
    if (id.classList.contains('d-flex')) {
        id.classList.remove('d-flex');
    }
    else {
        id.classList.add('d-flex');
    }
}

//  Logic of preloader
preloaderButton.onclick = function () {
    const textBlockShown = textBlock.classList.contains('d-flex');
    const preloaderShown = preloader.classList.contains('d-flex');

    if (!preloaderShown && textBlockShown) {
        showBlock(textBlock);
    } else if (!preloaderShown && !textBlockShown) {
        showBlock(preloader);
        setTimeout(function () {
            showBlock(textBlock);
            setTimeout(function () {
                showBlock(preloader);
            }, 1000);
        }, 2000);
    }
}

// Logic of Tabs
let lastTab;
function showTabText(id) {
    if (lastTab) {
        lastTab.classList.remove('selected');
        document.getElementById(lastTab.id+'-svg').classList.remove('selected');
    }
    const tab = document.getElementById(id);
    const tabImg = document.getElementById(id+'-svg');
    tab.classList.add('selected');
    tabImg.classList.add('selected');
    tabText.innerText = id;

    lastTab = tab;
}
