const
    preloaderButton = document.getElementById('preloaderButton'),
    preloader = document.getElementById('preloader'),
    textBlock = document.getElementById('textBlock'),
    tabText = document.getElementById('tabText');

//  Show or hide block
function showBlock(id) {
    if (id.classList.contains('d-flex')) {
        id.classList.remove('d-flex');
    }
    else {
        id.classList.add('d-flex');
    }
}

//  Logic of preloader
preloaderButton.onclick = function () {
    const textBlockShown = textBlock.classList.contains('d-flex');
    const preloaderShown = preloader.classList.contains('d-flex');

    if (!preloaderShown && textBlockShown) {
        showBlock(textBlock);
    } else if (!preloaderShown && !textBlockShown) {
        showBlock(preloader);
        setTimeout(function () {
            showBlock(textBlock);
            setTimeout(function () {
                showBlock(preloader);
            }, 1000);
        }, 2000);
    }
}

// Logic of Tabs
let lastTab;
function showTabText(id) {
    if (lastTab) {
        lastTab.classList.remove('selected');
        document.getElementById(lastTab.id+'-svg').classList.remove('selected');
    }
    const tab = document.getElementById(id);
    const tabImg = document.getElementById(id+'-svg');
    tab.classList.add('selected');
    tabImg.classList.add('selected');
    tabText.innerText = id;

    lastTab = tab;
}

const
    info = document.getElementById('info'),
    popover = document.getElementById('popoverBox');
    close = document.getElementById('close');

function showPopover() {
    popover.classList.add('visible');
}

function hidePopover() {
    popover.classList.remove('visible');
}

info.onclick = function () {
    showPopover();
};

close.onclick = function () {
    hidePopover();
};

class Timer {
    constructor(year, month, date) {
        this.year = year;
        this.month = month;
        this.date = date;

        setInterval(() => {
            this.updateTimer;
        }, 1000);
        this.timer = document.getElementById('timer');
        this.timer.innerHTML =
            '<div class="col-3"><div><span class="days"></span><span>days</span></div></div>' +
            '<div class="col-3"><div><span class="hours"></span><span>hours</span></div></div>' +
            '<div class="col-3"><div><span class="minutes"></span><span>minutes</span></div></div>' +
            '<div class="col-3"><div><span class="seconds"></span><span>seconds</span></div></div>';
        this.timer.classList.add('visible');
    }

    get arrayTimeLeft() {
        const
            timeLeft = new Date(this.year, this.month, this.date) - new Date(),
            seconds = Math.floor( timeLeft / 1000 % 60 ),
            minutes = Math.floor( timeLeft / 1000 / 60 % 60 ),
            hours = Math.floor( timeLeft / 1000 / 60 / 60 % 24),
            days = Math.floor( timeLeft / 1000 / 60 / 60 / 24);
        return {
            'minutes' : minutes,
            'hours' : hours,
            'days' : days,
            'seconds' : seconds
        }
    }

    get updateTimer() {
        const
            daysSpan = this.timer.querySelector('.days'),
            hoursSpan = this.timer.querySelector('.hours'),
            minutesSpan = this.timer.querySelector('.minutes'),
            secondsSpan = this.timer.querySelector('.seconds');

        daysSpan.innerHTML = String( this.arrayTimeLeft.days );
        hoursSpan.innerHTML = String( this.arrayTimeLeft.hours );
        minutesSpan.innerHTML = String( this.arrayTimeLeft.minutes );
        secondsSpan.innerHTML = String( this.arrayTimeLeft.seconds );
    }
}

const timer = new Timer(2020, 9-1, 1);

