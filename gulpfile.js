const gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    cssMin = require('gulp-cssmin'),
    shorthand = require('gulp-shorthand'),
    scss = require('gulp-sass'),
    scssLint = require('gulp-scss-lint'),
    pug = require('gulp-pug'),
    pugLinter = require('gulp-pug-linter'),
    htmlValidator = require('gulp-w3c-html-validator'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    browserSync = require('browser-sync').create(),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    del = require('del');

function styles() {
    return gulp.src('src/styles/scss/styles.scss')
        .pipe(plumber({errorHandler: notify.onError()}))
        .pipe(scssLint())
        .pipe(scss())
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(shorthand())
        .pipe(cssMin())
        .pipe(gulp.dest('src/styles/css/'))
        .pipe(notify('Successful convert .scss to .css !'))
        .pipe(browserSync.reload({stream:true}));
}

function scripts() {
    return gulp.src(['src/scripts/*.js', '!src/scripts/all.js', '!src/scripts/all-min.js'])
        .pipe(plumber({errorHandler: notify.onError()}))
        .pipe(concat('all.js'))
        .pipe(gulp.dest('src/scripts/'))
        .pipe(minify())
        .pipe(gulp.dest('src/scripts/'))
        .pipe(notify('Successful convert scripts to all.min.js !'))
        .pipe(browserSync.reload({stream:true}));
}

function pages() {
    return gulp.src('src/pages/index.pug')
        .pipe(plumber({errorHandler: notify.onError()}))
        .pipe(pugLinter({reporter: 'default'}))
        .pipe(pug())
        .pipe(htmlValidator())
        .pipe(gulp.dest('./'))
        .pipe(notify('Successful convert .pug to .html !'))
        .pipe(browserSync.reload({stream:true}));
}

function watchFiles(cb) {
    gulp.watch('src/styles/scss/', gulp.series(styles));
    gulp.watch(['src/pages/*.pug', 'src/pages/*/*.pug'], gulp.series(pages));
    gulp.watch(['src/scripts/*.js', '!src/scripts/all.js', '!src/scripts/all-min.js'], gulp.series(scripts));
    cb()
}

function server(cb) {
    let files = [
        'index.html',
        'src/styles/css/style.css',
        'src/scripts/all.min.js',
        'src/images/'
    ];
    browserSync.init(files, {
        server: {
            baseDir: "./"
        },
        open: 'local',
        notify: false
    });
    cb();
}

function clean() {
    console.log('\t[notification] deleted index.html, styles.css, all.min.js');
 return del(['index.html', 'src/styles/css/styles.css', 'src/scripts/all.min.js', 'src/scripts/all.js']);
}

exports.default = gulp.series(clean, pages, styles, scripts, server, watchFiles);
