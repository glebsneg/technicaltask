glebsneg
========


Install
-------

###### Step 1
Open a terminal and navigate to the desired install directory.
```shell
$ git clone https://gitlab.com/glebsneg/technicaltask.git
```
Please note that this gives you the current development version of Landing, what is likely *unstable* and *not ready for production use*!

###### Step 2

```shell
$ npm i
```

Requires:
gulp
    CLI version: 2.2.0
    Local version: 4.0.2

Install all dependence

###### Step 3

Run

```shell
$ npm run gulp
```
